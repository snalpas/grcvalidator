<?php

/**
 * Function grcValidator
 *
 * verify google reCaptcha 2.0 response
 *
 * @param string $secret
 * @param string $response
 */

function grcValidator($secret = null, $response = null)
{
  if(!$secret)
    return null;

  if(!$response)
    return 0;

  if($verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response")
  and json_decode($verify)->success)
    return true;

  return false;
}

function grcValidatorExplicit($secret = null, $response = null)
{
  if(!$secret)
    return "missing secret";

  if(!$response)
    return "missing response";

  if($verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response")
  and json_decode($verify)->success)
    return true;

  if(isset($verify->{"error-codes"}) and in_array("invalid-input-secret", $verify->{"error-codes"}))
    return "invalid secret";

  return false;
}
