Simple reCAPTCHA 2.0 Validator
============

A validator for [Google's reCaptcha 2.0](https://developers.google.com/recaptcha/intro) responses in a **simple** PHP function.

Installation
------------

#### Composer (Recommended)
To add this dependency using the command, run the following from within your
project directory:
```
composer require nalpas/grcvalidator
```
or directly in your composer.json file
```
"require": {
    "nalpas/grcvalidator": "*"
}
```

#### Direct download (no Composer)
If you wish to install the library manually (i.e. without Composer), then you can use the links on the main project page to either clone the repo or download the [ZIP file](https://github.com/nalpas/grecaptcha-validator/archive/master.zip) and require it into your script. For example:
```
require('/path/to/recaptcha/src/grcvalidator.php');
```

Usage
------------

| Arguments | description |
| :--- |:--- |
| **$secret** | [Your Google's reCaptcha API secret code](https://www.google.com/recaptcha/admin#list) |
| **$response** | Google's reCaptcha code sent by the user |

####Default

```php
$result = grcValidator($secret, $response);

if($result){
  // That's all right
}
```

####Explicit

```php
$result = grcValidatorExplicit($secret, $response);

if($result === true){
  // That's all right
}
```

####$result possibilities

| Default | Explicit | Reason |
| :--- | :--- | :--- |
| **true** | **true** | That's all right |
| **null** | "missing secret" | $secret is undefined |
| **0** | "missing response" | $secret is undefined |
| **false** | "invalid secret" | $secret is invalid by Google |
| **false** | **false** | Something else is wrong |
